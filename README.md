SyncBlock
---

Synchronize Minecraft versions, mods, and profiles from a central master.

Concepts
---

## Versions

A Minecraft version consists of a folder in .minecraft/versions, which in turn contains
a jar file and a json file which describes the version.
